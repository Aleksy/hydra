import jdk.nashorn.api.scripting.ScriptObjectMirror;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class JsAlgorithm implements Algorithm {
   private String filename;

   @Override
   public String move(State state) throws FileNotFoundException, ScriptException {
      int[] arrayState = AlgorithmUtil.parseGameStateToArray(state);
      ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
      ScriptObjectMirror result = (ScriptObjectMirror) engine.eval(
         generateInput(arrayState) + readScript());
      int[] parsedResult = new int[2];
      parsedResult[0] = (int) result.get("0");
      parsedResult[1] = (int) result.get("1");
      return AlgorithmUtil.parseAiOutput(parsedResult);
   }

   private static String generateInput(int[] state) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("var state = [");
      for (int i = 0; i < 16; i++) {
         stringBuilder.append(state[i]);
         if (i != 15)
            stringBuilder.append(", ");
      }
      stringBuilder.append("];\n");
      return stringBuilder.toString();
   }

   @Override
   public void setSource(String filename) {
      this.filename = filename;
   }

   @Override
   public boolean isJsAlgorithm() {
      return true;
   }


   private String readScript() throws FileNotFoundException {
      Scanner scanner = new Scanner(new File("src/" + filename + ".js"));
      StringBuilder stringBuilder = new StringBuilder();
      while (scanner.hasNextLine()) {
         String line = scanner.nextLine();
         stringBuilder.append(line).append("\n");
      }
      return stringBuilder.toString();
   }
}
