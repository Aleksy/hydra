import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MinMaxTest {
   public static int[] move(int[] state) {
      if (state.length != 16)
         throw new IllegalArgumentException("Length of the state array should equal 16.");
      int[] weights = {1, 1, 1, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, 1, 1, 1};
      for (int i = 0; i < 16; i++) {
         weights = checkDownVertical(state, i, weights);
         weights = checkUpVertical(state, i, weights);
         weights = checkLeftHorizontal(state, i, weights);
         weights = checkRightHorizontal(state, i, weights);
         weights = checkUpRight(state, i, weights);
         weights = checkUpLeft(state, i, weights);
         weights = checkDownRight(state, i, weights);
         weights = checkDownLeft(state, i, weights);
      }
      for (int i = 0; i < 16; i++) {
         if (state[i] != 0)
            weights[i] = -1;
      }
      int theHighestWeight = -1;
      for (int i = 0; i < 16; i++) {
         if (weights[i] > theHighestWeight) {
            theHighestWeight = weights[i];
         }
      }
      List<Integer> indexesWithTheHighestWeight = new ArrayList<>();
      for (int i = 0; i < 16; i++) {
         if (weights[i] == theHighestWeight) {
            indexesWithTheHighestWeight.add(i);
         }
      }
      Random random = new Random();
      int choice = indexesWithTheHighestWeight.get(random.nextInt(indexesWithTheHighestWeight.size()));
      int[] xy = new int[2];
      xy[1] = choice % 4;
      xy[0] = choice / 4;
      return xy;
   }

   private static int[] checkDownLeft(int[] status, int index, int[] weights) {
      int i1 = down(left(index));
      int i2 = down(left(i1));
      if (i1 == -1 || i2 == -1)
         return weights;
      int f1 = status[i1];
      int f2 = status[i2];
      if (f1 == 2 && f2 == 2)
         weights[index] += 4;
      return weights;
   }


   private static int[] checkDownRight(int[] status, int index, int[] weights) {
      int i1 = down(right(index));
      int i2 = down(right(i1));
      if (i1 == -1 || i2 == -1)
         return weights;
      int f1 = status[i1];
      int f2 = status[i2];
      if (f1 == 2 && f2 == 2)
         weights[index] += 4;
      return weights;
   }


   private static int[] checkUpLeft(int[] status, int index, int[] weights) {
      int i1 = up(left(index));
      int i2 = up(left(i1));
      if (i1 == -1 || i2 == -1)
         return weights;
      int f1 = status[i1];
      int f2 = status[i2];
      if (f1 == 2 && f2 == 2)
         weights[index] += 4;
      return weights;
   }


   private static int[] checkUpRight(int[] status, int index, int[] weights) {
      int i1 = up(right(index));
      int i2 = up(right(i1));
      if (i1 == -1 || i2 == -1)
         return weights;
      int f1 = status[i1];
      int f2 = status[i2];
      if (f1 == 2 && f2 == 2)
         weights[index] += 4;
      return weights;
   }

   private static int[] checkRightHorizontal(int[] status, int index, int[] weights) {
      int i1 = right(index);
      int i2 = right(i1);
      if (i1 == -1 || i2 == -1)
         return weights;
      int f1 = status[i1];
      int f2 = status[i2];
      if (f1 == 2 && f2 == 2)
         weights[index] += 4;
      return weights;
   }

   private static int[] checkLeftHorizontal(int[] status, int index, int[] weights) {
      int i1 = left(index);
      int i2 = left(i1);
      if (i1 == -1 || i2 == -1)
         return weights;
      int f1 = status[i1];
      int f2 = status[i2];
      if (f1 == 2 && f2 == 2)
         weights[index] += 4;
      return weights;
   }

   private static int[] checkUpVertical(int[] status, int index, int[] weights) {
      int i1 = up(index);
      int i2 = up(i1);
      if (i1 == -1 || i2 == -1)
         return weights;
      int f1 = status[i1];
      int f2 = status[i2];
      if (f1 == 2 && f2 == 2)
         weights[index] += 4;
      return weights;
   }

   private static int[] checkDownVertical(int[] status, int index, int[] weights) {
      int i1 = down(index);
      int i2 = down(i1);
      if (i1 == -1 || i2 == -1)
         return weights;
      int f1 = status[i1];
      int f2 = status[i2];
      if (f1 == 2 && f2 == 2)
         weights[index] += 4;
      return weights;
   }

   private static int up(int index) {
      if (index == -1)
         return -1;
      return index - 4 < 0 ? -1 : index - 4;
   }

   private static int down(int index) {
      if (index == -1)
         return -1;
      return index + 4 > 15 ? -1 : index + 4;
   }

   private static int left(int index) {
      if (index == 0 || index == 4 || index == 8 || index == 12 || index == -1)
         return -1;
      return index - 1;
   }

   private static int right(int index) {
      if (index == 3 || index == 7 || index == 11 || index == 15 || index == -1)
         return -1;
      return index + 1;
   }
}
