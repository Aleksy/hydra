public enum Player {
   NO_PLAYER(' ', 0),
   FIRST('x', 1),
   SECOND('o', 2);

   private char value;
   private int number;

   Player(char value, int number) {
      this.value = value;
      this.number = number;
   }

   public static Player getByValue(int value) {
      switch (value) {
         case 0:
            return Player.NO_PLAYER;
         case 1:
            return Player.FIRST;
         case 2:
            return Player.SECOND;
      }
      return null;
   }

   public int getNumber() {
      return number;
   }

   public char getValue() {
      return value;
   }
}
