public class Field {
   private int x;
   private int y;
   private Player player;

   public Field(int x, int y) {
      this.x = x;
      this.y = y;
      this.player = Player.NO_PLAYER;
   }

   public int getX() {
      return x;
   }

   public int getY() {
      return y;
   }

   public Player getPlayer() {
      return player;
   }

   public void setPlayer(Player player) {
      this.player = player;
   }

   public void clear() {
      this.player = Player.NO_PLAYER;
   }
}
