import javax.script.ScriptException;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

/**
 * The game class
 */
public class Game {
   private State state;
   private boolean ended;
   private Player winner;
   private Scanner scanner;
   private Algorithm algorithm;

   /**
    * The constructor
    */
   public Game() {
      this.state = State.initializeEmpty();
      this.ended = false;
      this.winner = null;
      this.scanner = new Scanner(System.in);
      algorithm = chooseRandom();
   }

   /**
    * Is ended
    *
    * @return is ended
    */
   public boolean isEnded() {
      return ended;
   }

   /**
    * Getter for winner
    *
    * @return winner
    */
   public Player getWinner() {
      return winner;
   }

   /**
    * Starts the game
    *
    * @return winner
    * @throws GameEndedException if was already ended
    */
   public Player start() throws GameEndedException, ScriptException, FileNotFoundException, InterruptedException {
      if (ended) {
         throw new GameEndedException();
      } else {
         int tour = 0;
         while (!ended) {
            makeTour(1 + tour++ % 2);
         }
      }
      display();
      return winner;
   }

   private void makeTour(int playerNumber) throws ScriptException, FileNotFoundException, InterruptedException {
      display();
      System.out.print("\nMove: ");
      Coords parsed;
      while (true) {
         String input = readInput(playerNumber);
         if (configuration(input)) {
            changeAlgorithm(
               input.substring(0, input.indexOf(":")),
               input.substring(input.indexOf(":") + 1));
         }
         if (playerNumber == 2 && !configuration(input)) {
            System.out.print(input + " [AI]\n");
         }
         parsed = parse(input);
         if (parsed == null) {
            if (!configuration(input))
               System.out.print("Wrong input: ");
            else
               System.out.print("Algorithm changed. Your move: ");
         } else {
            break;
         }
      }
      state.getFieldAt(parsed.getX() - 1, parsed.getY() - 1)
         .setPlayer(Player.getByValue(playerNumber));
      if (validateWinner()) {
         end(Player.getByValue(playerNumber));
      }
   }

   private boolean configuration(String input) {
      if (input.startsWith("js:") || input.startsWith("c:"))
         return true;
      return false;
   }

   private String readInput(int playerNumber) throws ScriptException, FileNotFoundException {
      if (playerNumber == 1) {
         return scanner.nextLine();
      } else return algorithm.move(state);
   }

   private boolean validateWinner() {
      boolean horizontal = validateHorizontal();
      boolean vertical = validateVertical();
      boolean cross = validateCross();
      return horizontal || vertical || cross;
   }

   private boolean validateCross() {
      Field f1 = state.getFieldAt(0, 0);
      Field f2 = state.getFieldAt(1, 1);
      Field f3 = state.getFieldAt(2, 2);
      Field f4 = state.getFieldAt(3, 3);
      if (f1.getPlayer() == f2.getPlayer() &&
         f2.getPlayer() == f3.getPlayer() &&
         f1.getPlayer() != Player.NO_PLAYER ||
         f2.getPlayer() == f3.getPlayer()
            && f3.getPlayer() == f4.getPlayer() &&
            f2.getPlayer() != Player.NO_PLAYER)
         return true;
      f1 = state.getFieldAt(0, 3);
      f2 = state.getFieldAt(1, 2);
      f3 = state.getFieldAt(2, 1);
      f4 = state.getFieldAt(3, 0);
      if (f1.getPlayer() == f2.getPlayer() &&
         f2.getPlayer() == f3.getPlayer() &&
         f1.getPlayer() != Player.NO_PLAYER ||
         f2.getPlayer() == f3.getPlayer() &&
            f3.getPlayer() == f4.getPlayer() &&
            f2.getPlayer() != Player.NO_PLAYER)
         return true;
      f1 = state.getFieldAt(0, 1);
      f2 = state.getFieldAt(1, 2);
      f3 = state.getFieldAt(2, 3);
      if (f1.getPlayer() == f2.getPlayer() &&
         f2.getPlayer() == f3.getPlayer() &&
         f1.getPlayer() != Player.NO_PLAYER)
         return true;
      f1 = state.getFieldAt(0, 2);
      f2 = state.getFieldAt(1, 1);
      f3 = state.getFieldAt(2, 0);
      if (f1.getPlayer() == f2.getPlayer() &&
         f2.getPlayer() == f3.getPlayer() &&
         f1.getPlayer() != Player.NO_PLAYER)
         return true;
      f1 = state.getFieldAt(1, 0);
      f2 = state.getFieldAt(2, 1);
      f3 = state.getFieldAt(3, 2);
      if (f1.getPlayer() == f2.getPlayer() &&
         f2.getPlayer() == f3.getPlayer() &&
         f1.getPlayer() != Player.NO_PLAYER)
         return true;
      f1 = state.getFieldAt(1, 3);
      f2 = state.getFieldAt(2, 2);
      f3 = state.getFieldAt(3, 1);
      return f1.getPlayer() == f2.getPlayer() &&
         f2.getPlayer() == f3.getPlayer() &&
         f1.getPlayer() != Player.NO_PLAYER;
   }

   private boolean validateVertical() {
      for (int i = 0; i < 4; i++) {
         for (int j = 0; j < 2; j++) {
            Field f1 = state.getFieldAt(i, j);
            Field f2 = state.getFieldAt(i, j + 1);
            Field f3 = state.getFieldAt(i, j + 2);
            if (
               f1.getPlayer() == f2.getPlayer() &&
                  f2.getPlayer() == f3.getPlayer() &&
                  f1.getPlayer() != Player.NO_PLAYER) {
               return true;
            }
         }
      }
      return false;
   }

   private boolean validateHorizontal() {
      for (int i = 0; i < 4; i++) {
         for (int j = 0; j < 2; j++) {
            Field f1 = state.getFieldAt(j, i);
            Field f2 = state.getFieldAt(j + 1, i);
            Field f3 = state.getFieldAt(j + 2, i);
            if (
               f1.getPlayer() == f2.getPlayer() &&
                  f2.getPlayer() == f3.getPlayer() &&
                  f1.getPlayer() != Player.NO_PLAYER) {
               return true;
            }
         }
      }
      return false;
   }

   private Coords parse(String input) {
      if (input == null)
         return null;
      if (input.length() != 2)
         return null;
      int x;
      int y;
      try {
         x = Integer.parseInt(input.substring(0, 1));
         y = Integer.parseInt(input.substring(1, 2));
      } catch (NumberFormatException e) {
         return null;
      }
      if (x < 1 || x > 4 || y < 1 || y > 4)
         return null;
      if (!state.getFieldAt(x - 1, y - 1).getPlayer().equals(Player.NO_PLAYER))
         return null;
      return new Coords(x, y);
   }

   private void end(Player player) {
      this.winner = player;
      scanner.close();
      ended = true;
   }

   private void changeAlgorithm(String parameter, String filename) {
      if ("js".equals(parameter)) {
         this.algorithm = new JsAlgorithm();
         this.algorithm.setSource(filename);
      }
      if ("c".equals(parameter)) {
         this.algorithm = new CppAlgorithm();
         this.algorithm.setSource(filename);
      }
   }

   private Algorithm chooseRandom() {
      int alg = new Random().nextInt(4);
      Algorithm algorithm = null;
      switch (alg) {
         case 0: {
            algorithm = new CppAlgorithm();
            algorithm.setSource("random");
            System.out.println("Choosen algorithm: random.dll");
            break;
         }
         case 1: {
            algorithm = new CppAlgorithm();
            algorithm.setSource("minmax");
            System.out.println("Choosen algorithm: minmax.dll");
            break;
         }
         case 2: {
            algorithm = new JsAlgorithm();
            algorithm.setSource("random");
            System.out.println("Choosen algorithm: random.js");
            break;
         }
         case 3: {
            algorithm = new JsAlgorithm();
            algorithm.setSource("minmax");
            System.out.println("Choosen algorithm: minmax.js");
            break;
         }
      }
      return algorithm;
   }

   private void display() {
      state.draw();
   }
}
