#include "CppAlgorithm.h"
#include <ctype.h>
#include <string.h>
#include <iostream>
#include <array>
#include <vector>
#include <ctime>

void printGameArrayValues(std::array<int, 16> array);
int up(int index);
int down(int index);
int left(int index);
int right(int index);
int right(int index);
std::array<int, 16> checkDownVertical(jint* statusFill, int index, std::array<int, 16> weights);
std::array<int, 16> checkUpVertical(jint* statusFill, int index, std::array<int, 16> weights);
std::array<int, 16> checkLeftHorizontal(jint* statusFill, int index, std::array<int, 16> weights);
std::array<int, 16> checkRightHorizontal(jint* statusFill, int index, std::array<int, 16> weights);
std::array<int, 16> checkUpRight(jint* statusFill, int index, std::array<int, 16> weights);
std::array<int, 16> checkUpLeft(jint* statusFill, int index, std::array<int, 16> weights);
std::array<int, 16> checkDownRight(jint* statusFill, int index, std::array<int, 16> weights);
std::array<int, 16> checkDownLeft(jint* statusFill, int index, std::array<int, 16> weights);

JNIEXPORT jintArray JNICALL _Java_CppAlgorithm_cppMove(JNIEnv * env, jobject obj, jintArray status)
{
      jintArray xy = env->NewIntArray(2);
      jint* statusFill = env->GetIntArrayElements(status, 0);
      jint pointFill[2];
      pointFill[0] = 0;
      pointFill[1] = 0;

      std::array<int, 16> weights = { 1, 1, 1, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, 1, 1, 1 };

      for(int i = 0; i < 16; i++)
      {
          weights = checkDownVertical(statusFill, i, weights);
          weights = checkUpVertical(statusFill, i, weights);
          weights = checkLeftHorizontal(statusFill, i, weights);
          weights = checkRightHorizontal(statusFill, i, weights);
          weights = checkUpRight(statusFill, i, weights);
          weights = checkUpLeft(statusFill, i, weights);
          weights = checkDownRight(statusFill, i, weights);
          weights = checkDownLeft(statusFill, i, weights);

      }

      for(int i = 0; i < 16; i++)
      {
          if(statusFill[i] != 0)
             weights[i] = -1;
      }

      int theHighestWeight = -1;

      for (int i = 0; i < 16; i++)
      {
         if (weights[i] > theHighestWeight)
         {
            theHighestWeight = weights[i];
         }
      }

      int numberOfIndexesOfTheHighestWeight = 0;

      for (int i = 0; i < 16; i++)
      {
         if (weights[i] == theHighestWeight)
         {
            numberOfIndexesOfTheHighestWeight++;
         }
      }

      std::array<int, 16> indexesWithTheHighestWeight = {};

      int j = 0;

      for (int i = 0; i < 16; i++)
      {
         if (weights[i] == theHighestWeight)
         {
            indexesWithTheHighestWeight[j++] = i;
         }
      }

      srand(time( NULL ));

      int choice = indexesWithTheHighestWeight[rand() % numberOfIndexesOfTheHighestWeight];
      pointFill[1] = choice % 4;
      pointFill[0] = choice / 4;

      env->SetIntArrayRegion(xy, 0, 2, pointFill);
      return xy;
}

int up(int index)
{

    if (index == -1 || index - 4 < 0)
    {
        return -1;
    }
    return index - 4;
}

int down(int index)
{
    if (index == -1 || index - 4 < 0)
    {
        return -1;
    }
    return index - 4;
}


int left(int index)
{
    if (index == 0 || index == 4 || index == 8 || index == 12 || index == -1)
    {
        return -1;
    }
    return index - 1;
}

int right(int index)
{
    if (index == 3 || index == 7 || index == 11 || index == 15 || index == -1)
    {
        return -1;
    }
    return index + 1;
}

std::array<int, 16> checkDownVertical(jint* statusFill, int index, std::array<int, 16> weights)
{
    int i1 = down(index);
    int i2 = down(i1);
    if(i1 == -1 || i2 == -1)
    {
        return weights;
    }
    int f1 = statusFill[i1];
    int f2 = statusFill[i2];
    if (f1 == 2 && f2 == 2)
    {
        weights[index] = weights[index] + 4;
    }
    return weights;
}

std::array<int, 16> checkUpVertical(jint* statusFill, int index, std::array<int, 16> weights)
{
    int i1 = up(index);
    int i2 = up(i1);
    if(i1 == -1 || i2 == -1)
    {
        return weights;
    }
    int f1 = statusFill[i1];
    int f2 = statusFill[i2];
    if (f1 == 2 && f2 == 2)
    {
        weights[index] = weights[index] + 4;
    }
    return weights;
}

std::array<int, 16> checkLeftHorizontal(jint* statusFill, int index, std::array<int, 16> weights)
{
    int i1 = left(index);
    int i2 = left(i1);
    if(i1 == -1 || i2 == -1)
    {
        return weights;
    }
    int f1 = statusFill[i1];
    int f2 = statusFill[i2];
    if (f1 == 2 && f2 == 2)
    {
        weights[index] = weights[index] + 4;
    }
    return weights;
}

std::array<int, 16> checkRightHorizontal(jint* statusFill, int index, std::array<int, 16> weights)
{
    int i1 = right(index);
    int i2 = right(i1);
    if(i1 == -1 || i2 == -1)
    {
        return weights;
    }
    int f1 = statusFill[i1];
    int f2 = statusFill[i2];
    if (f1 == 2 && f2 == 2)
    {
        weights[index] = weights[index] + 4;
    }
    return weights;
}

std::array<int, 16> checkUpRight(jint* statusFill, int index, std::array<int, 16> weights)
{
    int i1 = up(right(index));
    int i2 = up(right(i1));
    if(i1 == -1 || i2 == -1)
    {
        return weights;
    }
    int f1 = statusFill[i1];
    int f2 = statusFill[i2];
    if (f1 == 2 && f2 == 2)
    {
        weights[index] = weights[index] + 4;
    }
    return weights;
}

std::array<int, 16> checkUpLeft(jint* statusFill, int index, std::array<int, 16> weights)
{
    int i1 = up(left(index));
    int i2 = up(left(i1));
    if(i1 == -1 || i2 == -1)
    {
        return weights;
    }
    int f1 = statusFill[i1];
    int f2 = statusFill[i2];
    if (f1 == 2 && f2 == 2)
    {
        weights[index] = weights[index] + 4;
    }
    return weights;
}

std::array<int, 16> checkDownRight(jint* statusFill, int index, std::array<int, 16> weights)
{
    int i1 = down(right(index));
    int i2 = down(right(i1));
    if(i1 == -1 || i2 == -1)
    {
        return weights;
    }
    int f1 = statusFill[i1];
    int f2 = statusFill[i2];
    if (f1 == 2 && f2 == 2)
    {
        weights[index] = weights[index] + 4;
    }
    return weights;
}


std::array<int, 16> checkDownLeft(jint* statusFill, int index, std::array<int, 16> weights)
{
    int i1 = down(left(index));
    int i2 = down(left(i1));
    if(i1 == -1 || i2 == -1)
    {
        return weights;
    }
    int f1 = statusFill[i1];
    int f2 = statusFill[i2];
    if (f1 == 2 && f2 == 2)
    {
        weights[index] = weights[index] + 4;
    }
    return weights;
}

void printGameArrayValues(std::array<int,16> array)
{
    for(int i=0; i<array.size(); i++)
        {
            std::cout << array[i]  << " ";
            if (((i+1) % 4) == 0)
            {
                std::cout << "\n";
            }
        }
}