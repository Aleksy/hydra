import javax.script.ScriptException;
import java.io.FileNotFoundException;

public interface Algorithm {

   String move(State state) throws FileNotFoundException, ScriptException;

   void setSource(String filename);

   boolean isJsAlgorithm();
}
