import javax.script.ScriptException;
import java.io.FileNotFoundException;

public class CppAlgorithm implements Algorithm {
   private String source;

   @Override
   public String move(State state) throws FileNotFoundException, ScriptException {
      System.loadLibrary("src/" + source);
      int[] ints = this.cppMove(AlgorithmUtil.parseGameStateToArray(state));
      return AlgorithmUtil.parseAiOutput(ints);
   }

   private native int[] cppMove(int[] state);

   @Override
   public void setSource(String source) {
      this.source = source;
   }

   @Override
   public boolean isJsAlgorithm() {
      return false;
   }
}
