import javax.script.ScriptException;
import java.io.FileNotFoundException;

public class Main {

   public static void main(String[] args) throws ScriptException, GameEndedException, FileNotFoundException, InterruptedException {
      Player winner = new Game().start();
      System.out.println("Winner: " + winner);
   }
}
