import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class State {
   private List<Field> fields;

   private State(List<Field> fields) {
      this.fields = fields;
   }

   public Field getFieldAt(int x, int y) {
      Optional<Field> found = fields.stream()
         .filter(field -> field.getX() == x && field.getY() == y)
         .findFirst();
      return found.orElse(null);
   }

   public void clear() {
      this.fields.forEach(Field::clear);
   }

   public void draw() {
      System.out.printf("%c%c%c%c%c%c%c%c%c\n", 0x2554, 0x2550, 0x2566,
         0x2550, 0x2566, 0x2550, 0x2566, 0x2550, 0x2557);
      for (int y = 0; y < 4; y++) {
         for (int x = 0; x < 4; x++) {
            System.out.printf("%c", 0x2551);
            Field field = getFieldAt(x, y);
            System.out.print(field.getPlayer().getValue());
         }
         System.out.printf("%c\n", 0x2551);
         if (y != 3) {
            System.out.printf("%c%c%c%c%c%c%c%c%c\n", 0x2560, 0x2550, 0x256C,
               0x2550, 0x256C, 0x2550, 0x256C, 0x2550, 0x2563);
         }
      }
      System.out.printf("%c%c%c%c%c%c%c%c%c\n", 0x255A, 0x2550, 0x2569,
         0x2550, 0x2569, 0x2550, 0x2569, 0x2550, 0x255D);
   }

   public static State initializeEmpty() {
      List<Field> fields = new ArrayList<>();
      for (int x = 0; x < 4; x++) {
         for (int y = 0; y < 4; y++) {
            fields.add(new Field(x, y));
         }
      }
      return new State(fields);
   }
}
