public class AlgorithmUtil {
   public static String parseAiOutput(int[] point) {
      point[0] += 1;
      point[1] += 1;
      return point[0] + "" + point[1];
   }

   public static int[] parseGameStateToArray(State state) {
      int[] s = new int[16];
      int index = 0;
      for (int x = 0; x < 4; x++) {
         for (int y = 0; y < 4; y++) {
            s[index++] = state.getFieldAt(x, y).getPlayer().getNumber();
         }
      }
      return s;
   }

}
