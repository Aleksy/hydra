public class GameEndedException extends Exception {
   public GameEndedException() {
      super("The game was already ended! ");
   }
}
