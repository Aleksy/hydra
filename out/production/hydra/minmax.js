var weights = [1, 1, 1, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, 1, 1, 1]

function up(index) {
    if (index === -1) {
        return -1;
    }
    return index - 4 < 0 ? -1 : index - 4;
}

function down(index) {
    if (index === -1) {
        return -1;
    }
    return index + 4 > 15 ? -1 : index + 4;
}

function left(index) {
    if (index === 0 || index === 4 || index === 8 || index === 12 || index === -1) {
        return -1;
    }
    return index - 1;
}

function right(index) {
    if (index === 3 || index === 7 || index === 11 || index === 15 || index === -1) {
        return -1;
    }
    return index + 1;
}

function checkDownLeft(status, index, weights) {
    var i1 = down(left(index));
    var i2 = down(left(i1));
    if(i1 === -1 || i2 === -1) {
        return weights;
    }
    var f1 = status[i1];
    var f2 = status[i2];
    if (f1 === 2 && f2 === 2) {
        weights[index] += 4;
    }
    return weights;
}

function checkDownRight(status, index, weights) {
    var i1 = down(right(index));
    var i2 = down(right(i1));
    if(i1 === -1 || i2 === -1) {
        return weights;
    }
    var f1 = status[i1];
    var f2 = status[i2];
    if (f1 === 2 && f2 === 2) {
        weights[index] += 4;
    }
    return weights;
}

function checkUpLeft(status, index, weights) {
    var i1 = up(left(index));
    var i2 = up(left(i1));
    if(i1 === -1 || i2 === -1) {
        return weights;
    }
    var f1 = status[i1];
    var f2 = status[i2];
    if (f1 === 2 && f2 === 2) {
        weights[index] += 4;
    }
    return weights;
}

function checkUpRight(status, index, weights) {
    var i1 = up(right(index));
    var i2 = up(right(i1));
    if(i1 === -1 || i2 === -1) {
        return weights;
    }
    var f1 = status[i1];
    var f2 = status[i2];
    if (f1 === 2 && f2 === 2) {
        weights[index] += 4;
    }
    return weights;
}

function checkRightHorizontal(status, index, weights) {
    var i1 = right(index);
    var i2 = right(i1);
    if(i1 === -1 || i2 === -1) {
        return weights;
    }
    var f1 = status[i1];
    var f2 = status[i2];
    if (f1 === 2 && f2 === 2) {
        weights[index] += 4;
    }
    return weights;
}

function checkLeftHorizontal(status, index, weights) {
    var i1 = left(index);
    var i2 = left(i1);
    if(i1 === -1 || i2 === -1) {
        return weights;
    }
    var f1 = status[i1];
    var f2 = status[i2];
    if (f1 === 2 && f2 === 2) {
        weights[index] += 4;
    }
    return weights;
}

function checkUpVertical(status, index, weights) {
    var i1 = up(index);
    var i2 = up(i1);
    if(i1 === -1 || i2 === -1) {
        return weights;
    }
    var f1 = status[i1];
    var f2 = status[i2];
    if (f1 === 2 && f2 === 2) {
        weights[index] += 4;
    }
    return weights;
}

function checkDownVertical(status, index, weights) {
    var i1 = down(index);
    var i2 = down(i1);
    if(i1 === -1 || i2 === -1) {
        return weights;
    }
    var f1 = status[i1];
    var f2 = status[i2];
    if (f1 === 2 && f2 === 2) {
        weights[index] += 4;
    }
    return weights;
}

for(var i = 0; i < 16; i++) {
    weights = checkDownVertical(state, i, weights);
    weights = checkUpVertical(state, i, weights);
    weights = checkLeftHorizontal(state, i, weights);
    weights = checkRightHorizontal(state, i, weights);
    weights = checkUpRight(state, i, weights);
    weights = checkUpLeft(state, i, weights);
    weights = checkDownRight(state, i, weights);
    weights = checkDownLeft(state, i, weights);
}
for(var i = 0; i < 16; i++) {
    if(state[i] != 0)
       weights[i] = -1;
 }
 var theHighestWeight = -1;
 for (var i = 0; i < 16; i++) {
    if (weights[i] > theHighestWeight) {
       theHighestWeight = weights[i];
    }
 }
var indexesWithTheHighestWeight = [];
 for (var i = 0; i < 16; i++) {
    if (weights[i] === theHighestWeight) {
       indexesWithTheHighestWeight.push(i);
    }
 }

 var choice = indexesWithTheHighestWeight[Math.floor(Math.random() * indexesWithTheHighestWeight.length)];
 var xy = [0, 0];
 xy[1] = choice % 4;
 xy[0] = Math.floor(choice / 4);
 xy;